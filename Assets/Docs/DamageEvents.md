# Damage Events

## Damage Event
* Trigger damage events
* Record attacker and target actors

## Status Modifier Applied Event
* Add a new Status Effect component to the target

## Death Event
* Play damage animation and trigger death events on actor's death
* CombatManager records death event:
  * Actor that died
  * Actor giving final blow
* If EXP is given after battle, reward target's EXP reward to the attacker

## Revive Event
* If combat is still active and the target is dead, target will be brought back to life
* If not guaranteed health percentage, roll for 15-25% of target's health restored
* Trigger any revival events