# File Hierarchy

## Repository Project
```
Assets
|   Audio
|   Docs
|   EntityInfo
    └   Attacks
    └   Elements
    └   Enemies
|   Materials
|   Plugins
|   Prefabs
|   Scenes
|   Scripts
    └   Actors
    └   Attacks
    └   Combat
    └   Elements
    └   UI
    └   Utils
```

## Project Unitypackage
```
|   Docs
|   EntityInfo
    └   Attacks
    └   Elements
    └   Enemies
|   Scripts
    └   Actors
    └   Attacks
    └   Combat
    └   Elements
    └   UI
    └   Utils
```