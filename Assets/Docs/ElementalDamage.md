# Elemental Damage

**Actors** and **attacks** both have an Elemental modifier to them. This modifier determines whether an attack will affect the target actor. If so, will the attacking element be stronger or weaker than the target element?

***

## `Element.cs`

ScriptableObject that contains information about a specific element

## `ElementUtils.cs`

Contains static methods pertaining to the Element class

***

Still trying to figure out if there's a way to handle elemental advantages/disadvantages via a chart or tree.