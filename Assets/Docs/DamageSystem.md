# RPG Damage System

## Base Damage Component
* Apply damage on hit
* Keep track on actors giving and receiving damage
* Damage = Base damage range for attack
* Show received damage either above target or where target was hit

## Critical Hit Damage Component
* On successful hit, roll for crit chance based on attacker's crit rate
* if success, critical bonus =  damage * 20-30%
* add crit bonus to final damage

## Elemental Damage Component
* Elemental modifier -> is receiving damage stronger, weaker, or equal to the actor's elemental damage modifiers?
* If yes, automatic crit
* If no, apply crit bonus as ineffective modifier instead

## Status Modifier Damage Component
* eg: apply confusion, burn, frozen, etc
* Roll for attack's chance at inflicting status modifier
* If yes, apply to target

## Special Attack Damage Component
* Depending on fight system, guaranteed crit
