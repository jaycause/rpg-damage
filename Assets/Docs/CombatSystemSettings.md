# Combat System Settings

Depending on these global settings, these will determine the style of combat that'll take place in the game

Combat Types:

1. Turn-based
2. Live-action

Perspective

1. Third-person
2. First-person

***

## Turn-based Action

The player will take turns or phases with the participating actors in attacking.

On the player's turn, they will be presented with a menu of actions to choose from. Once selected, the player will end their turn and perform their action.

### How turns are determined

There are two ways to determine the attacking order:
1. #### Speed stats of actors
    At the start of combat, the order of attacks will go from fastest to slowest. If an actor's speed is modified during combat, this will affect their attacking order.
2. #### Round robin starting at attacking actor
   If the player approaches the enemy first or the enemy approaches the player first, then whoever approached first will have the first attack. Actors are then ordered clockwise or counterclockwise.

### Time limit of turns

The player's turn can last indefinitely, but an option can be set to add a timer to the player's turn.

## Live Action

There are no turns or waiting in this style of combat. All actors can attack at their will.

Since the player can attack whenever, their action options will not be shown on a meny and, instead, mapped to their controller buttons.

### Spacing out attacks

Since actors can attack as much as they want whenever, there needs to be a method of pacing and limiting attacks. This will usually be handled by an attack's cooldown.

***

TODO: figure how to set combat perspective upon the start of a project