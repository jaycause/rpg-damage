# Status Effects

Status Effects are events applied to a target character that applies an effect to them over time. They can be applied via attack or item.

## When Status Effects are Applied
If an attack can apply a status effect, then it'll roll for the chance to do so. If the status effect is coming from an item, then the status effect will be guaranteed to work.

## Status Effect Lifecycle

### Initializing Effect
On initializing the effect:

1. A new Status Effect component is added to the target character