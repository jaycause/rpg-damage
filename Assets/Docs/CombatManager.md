# Combat Manager

Records global combat events

### `private List<GameObject> enemyActors`
A list of all enemy actors in the combat instance

When an enemy dies, move their index to the `combatDeaths` dictionary

### `private Dictionary<GameObject, DeathObject> combatDeaths`
Records all deaths that happen in the combat instance with their designated target actor

Once combat is complete, distribute EXP to designated player actors

