﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Global MonoBehavior the manages the combat instance
/// </summary>
public class CombatManager : MonoBehaviour
{
#region Attributes
    //  TODO: add CombatSettings to switch combat types
    /// <summary>
    /// The current combat instance
    /// </summary>
    private static CombatManager combatInstance;
    /// <summary>
    /// The current instance's round manager
    /// </summary>
    private static RoundManager roundManager;
    /// <summary>
    /// The current instance's character manager
    /// </summary>
    private static CombatCharacterManager characterManager;
#endregion

#region Properties
    /// <summary>
    /// Returns this instance's round manager
    /// </summary>
    public RoundManager InstanceRoundManager => roundManager;
    /// <summary>
    /// Returns this instance's character manager
    /// </summary>
    public CombatCharacterManager InstanceCharacterManager => characterManager;
#endregion

#region Events
    public delegate void CombatInstanceEvent();
    public delegate void DeathEvent (DeathObject death);
    /// <summary>
    /// Event handler for starting the combat instance
    /// </summary>
    public static event CombatInstanceEvent OnCombatStart;
    /// <summary>
    /// Event handler for ending the combat instance
    /// </summary>
    public static event CombatInstanceEvent OnCombatEnd;
    /// <summary>
    /// Event handler for an enemy's death in combat
    /// </summary>
    public static event DeathEvent OnEnemyDeath;
    /// <summary>
    /// Event handler for a player's death in combat
    /// </summary>
    public static event DeathEvent OnPlayerDeath;
#endregion

#region MonoBehaviors
    private void Awake() {
        combatInstance = this;
        roundManager = gameObject.AddComponent<RoundManager>();
        characterManager = gameObject.AddComponent<CombatCharacterManager>();
    }
    private void OnEnable() {
        OnCombatStart += InstanceRoundManager.NextRound;
        //OnCombatEnd += InstanceCharacterManager.CleanupEnemies;
    }
    private void OnDisable() {
        //OnCombatEnd -= InstanceCharacterManager.CleanupEnemies;
        Destroy(roundManager);
    }
    private void Start() {
        StartCoroutine(CombatSetupCharacters());
    }
#endregion

#region Character Handling
    /// <summary>
    /// Registers the player on combat setup
    /// </summary>
    /// <param name="player">The player being registered</param>
    //  DEPRECATED: redundant
    /* public static void HandlePlayerSpawn (PlayerCharacter player)
    {
        combatInstance.InstanceCharacterManager.RegisterPlayer(player);
    } */
    /// <summary>
    /// Registers the enemy on combat setup
    /// </summary>
    /// <param name="enemy">The enemy being registered</param>
    //  DEPRECATED: redundant
    /* public static void HandleEnemySpawn (EnemyCharacter enemy)
    {
        combatInstance.InstanceCharacterManager.RegisterEnemy(enemy);
    } */

    /// <summary>
    /// Handles character registration on combat setup
    /// </summary>
    /// <param name="character">The character to be registered</param>
    public static void HandleSpawn (Character character)
    {
        if (character is EnemyCharacter)
            combatInstance.InstanceCharacterManager.RegisterEnemy(character as EnemyCharacter);
        else
            combatInstance.InstanceCharacterManager.RegisterPlayer(character as PlayerCharacter);
    }
    /// <summary>
    /// Handles character and death information registration on character death
    /// </summary>
    /// <param name="death">Death information to be registered</param>
    public static void HandleDeath (DeathObject death)
    {
        if (death.target is EnemyCharacter)
            OnEnemyDeath(death);
        else 
            OnPlayerDeath(death);
        combatInstance.UpdateCombatStatus();
    }

    //  TODO: handle instance on character reviving
    //  TODO: add cases for actor(s) fleeing

    /// <summary>
    /// Checks if a player is dead
    /// </summary>
    /// <returns>A bool if the player is dead or not</returns>
    private bool IsPlayerDead ()    //FIXME: this might be redundant with handling player death event?
    {
        return InstanceCharacterManager.playerDeath != null;
    }
#endregion

#region Combat Instance Handlind
    /// <summary>
    /// Starts combat by triggering OnCombatStart
    /// </summary>
    public void CombatStart ()
    {
        Debug.Log("<size=50>Combat Start!</size>");
        OnCombatStart();
        OnCombatStart -= InstanceRoundManager.NextRound;
    }
    /// <summary>
    /// Ends combat by triggering OnCombatEnd
    /// </summary>
    private void CombatEnd ()
    {
        OnCombatEnd();
        Debug.Log("<size=50>Combat End!</size>");
        
    }

    /// <summary>
    /// Updates the status of the combat instance
    /// </summary>
    private void UpdateCombatStatus ()  //FIXME: this might be redundant with handling player death event?
    {
        if (combatInstance.IsPlayerDead())
        {
            //    TODO: handle player death/game over event
        }
        if (IsCombatComplete())
            CombatEnd();
    }

    /// <summary>
    /// Checks if all enemies in the combat instance are dead, noting the end of the combat instance
    /// </summary>
    /// <returns>A bool if all enemies are dead or not</returns>
    private bool IsCombatComplete ()
    {
        Debug.Log("Current active enemies: " + InstanceCharacterManager.enemyActors.Count);
        return InstanceCharacterManager.enemyActors.Count == 0;
    }
#endregion

#region Coroutines
    /// <summary>
    /// Coroutine that waits until all characters in combat are registered before starting the combat
    /// </summary>
    public IEnumerator CombatSetupCharacters ()
    {
        yield return new WaitUntil ( () => InstanceCharacterManager.IsSpawnRegistrationComplete() );
        Debug.Log("All characters are registered");
        BroadcastMessage("CombatStart");
    }
#endregion
}
