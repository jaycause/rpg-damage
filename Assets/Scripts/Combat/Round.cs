﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// MonoBehavior that handles the character turn cycle
/// </summary>
public class Round : MonoBehaviour
{
    /// <summary>
    /// The characters in combat in turn order
    /// </summary>
    public Queue<Character> currentTurnQueue;

    /// <summary>
    /// The current turn this round
    /// </summary>
    private Turn currentTurn;
    //private Coroutine turnInstance;
    /// <summary>
    /// True if all characters have had their turn this round
    /// </summary>
    private bool isRoundComplete = false;

    /// <summary>
    /// Returns if all characters have had their turn this round
    /// </summary>
    public bool IsRoundComplete => isRoundComplete;
    /// <summary>
    /// Sets and returns the current turn for this round
    /// </summary>
    /// <value></value>
    public Turn CurrentTurn
    {
        get => currentTurn;
        set => currentTurn = value;
    }

    private void Awake() {
        currentTurnQueue = new Queue<Character>(RoundManager.turnQueue);
    }
    private void Start() {
        Debug.Log("<b>Round start</b>");
        NextTurn();
    }

    /// <summary>
    /// Sets the next character's turn in the queue
    /// </summary>
    public void NextTurn ()
    {
        if (currentTurnQueue.Count > 0)
        {
            if (!currentTurnQueue.Peek().gameObject.activeInHierarchy)
            {
                Debug.Log("<color=red>" + currentTurnQueue.Peek().gameObject.name + " is dead. Skipping turn </color>");
                currentTurnQueue.Dequeue();
                NextTurn();
            } else {
                CurrentTurn = currentTurnQueue.Dequeue().gameObject.AddComponent<Turn>();
                CurrentTurn.StartTurn();
            }
        }
        else
        {
            isRoundComplete = true;
        }
            
    }

    /// <summary>
    /// Waits until the round is complete before moving on to the next round
    /// </summary>
    public IEnumerator RoundInstance ()
    {
        yield return new WaitUntil (
            () => IsRoundComplete
        );
        BroadcastMessage("EndRound");
    }
}