﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Global MonoBehavior that manages the characters in a combat instance
/// </summary>
public class CombatCharacterManager : MonoBehaviour
{
#region Attributes
    /// <summary>
    /// List of enemy characters that are active in combat
    /// </summary>
    /// <typeparam name="EnemyCharacter">Reference to an enemy character</typeparam>
    public List<EnemyCharacter> enemyActors = new List<EnemyCharacter>();
    /// <summary>
    /// A dictionary that maps a dead character to their designated DeathObject
    /// </summary>
    /// <typeparam name="Character">Reference to the dead character</typeparam>
    /// <typeparam name="DeathObject">Information about the character's death</typeparam>
    public Dictionary<Character, DeathObject> combatDeaths = new Dictionary<Character, DeathObject>();
    /// <summary>
    /// An object that is set upon player death detailing their death
    /// </summary>
    public Tuple<bool, DeathObject> playerDeath;
#endregion

#region MonoBehaviors
    private void OnEnable() {
        CombatManager.OnEnemyDeath += RegisterEnemyDeath;
        CombatManager.OnPlayerDeath += RegisterPlayerDeath;
    }
    private void OnDisable() {
        CombatManager.OnEnemyDeath -= RegisterEnemyDeath;  
        CombatManager.OnPlayerDeath -= RegisterPlayerDeath;
    }
#endregion

#region Character Spawn Registration
    /// <summary>
    /// Registers a player on combat instance start
    /// </summary>
    /// <param name="player">The player character reference</param>
    public void RegisterPlayer (PlayerCharacter player)
    {
        GetComponent<CombatManager>().InstanceRoundManager.AddCharacterToQueue(player);
        player.transform.SetParent(this.transform);   //  TODO: handle combat instance in a new scene
    }
    /// <summary>
    /// Registers an enemy on combat instance start
    /// </summary>
    /// <param name="enemy">The enemy character reference</param>
    public void RegisterEnemy (EnemyCharacter enemy)
    {
        enemyActors.Add(enemy);
        GetComponent<CombatManager>().InstanceRoundManager.AddCharacterToQueue(enemy);
        enemy.transform.SetParent(this.transform);
    }
    /// <summary>
    /// Checks if all characters in combat are registered
    /// </summary>
    /// <returns>A bool if all characters in combat are registered</returns>
    public bool IsSpawnRegistrationComplete ()
    {
        var combatCharacters = GameObject.FindObjectsOfType<Character>();
        return combatCharacters.All(
            character => character.transform.parent == this.transform
        );
    }
    
    /// <summary>
    /// Coroutine that waits until all enemies in combat are registered before starting the combat
    /// </summary>
    // DEPRECATED: redundant
    /* public IEnumerator CharacterSpawnSetup ()
    {
        yield return new WaitUntil ( () => IsSpawnRegistrationComplete() );
        Debug.Log("All characters are registered");
        BroadcastMessage("CombatStart");
    } */
#endregion

#region Character Death Registration
    /// <summary>
    /// Adds the dead enemy and its death details to the combatDeaths list and removes that enemy from the active enemies list
    /// </summary>
    /// <param name="death">Details of the enemy's death</param>
    private void RegisterEnemyDeath (DeathObject death)
    {
        var enemyActorIndex = enemyActors.IndexOf(death.target as EnemyCharacter);
        combatDeaths.Add(enemyActors[enemyActorIndex], death);
        enemyActors.RemoveAt(enemyActorIndex);
    }
    /// <summary>
    /// Records details of the player's death and triggers the GameOver event
    /// </summary>
    /// <param name="death">Details of the player's death</param>
    private void RegisterPlayerDeath (DeathObject death)
    {
        playerDeath = new Tuple<bool, DeathObject>(true, death);
        //  TODO: trigger Game Over event
    }

    //  DEPRECATED: might be redundant
    // public void RegisterDeath (DeathObject death)
    // {
    //     if (death.target is EnemyCharacter)
    //         RegisterEnemyDeath(death);
    //     else
    //         RegisterPlayerDeath(death);
    // }
    
    //  TODO: Seperate instance cleanup methods to a seperate class?
    //  TODO: Add up and distribute EXP to player(s)
#endregion

#region Character Cleanup
    /// <summary>
    /// Destroys all enemy GameObjects when combat is complete
    /// </summary>
    public void CleanupEnemies ()
    {
        //  TODO: ensure this excludes player deaths
        foreach (var enemyDeath in combatDeaths)
        {
            Destroy(enemyDeath.Key.gameObject);
        }
    }
#endregion
}
