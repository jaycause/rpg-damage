﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurnType { RoundRobin, Time, Speed }

[CreateAssetMenu(fileName="Combat Settings", menuName="Combat Settings")]
public class CombatSettings : ScriptableObject
{
    public TurnType turnType;

}
