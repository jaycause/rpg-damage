﻿// DEPRECATED: might be redundant
public enum CharacterType { Player, Enemy }

// DEPRECATED: might be redundant
public enum ElementType
{
    None = 0,
    Light = 1,
    Dark = 2,
    Fire = 3,
    Water = 4,
    Earth = 5
}

// DEPRECATED: might be redundant
public enum HasElementalAdvantage { None = 0, Yes = 1, No = 2 }
