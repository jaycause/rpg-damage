﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Information about successful damage landed
/// </summary>
public class DamageObject
{
    /// <summary>
    /// Character that dealt the damage
    /// </summary>
    private Character ownerActor;
    /// <summary>
    /// Character that's receiving the damage
    /// </summary>
    private Character targetActor;
    /// <summary>
    /// Attack used on successful damage
    /// </summary>
    private Attack attack;
    /// <summary>
    /// Is the damage dealt critical?
    /// </summary>
    public bool isAttackCritical;
    /// <summary>
    /// Amount of damage applied to target
    /// </summary>
    private int damagePoints;

    /// <summary>
    /// Returns the character that dealt the damage
    /// </summary>
    public Character owner => ownerActor;
    /// <summary>
    /// Returns the character that's receiving the damage
    /// </summary>
    public Character target => targetActor;
    /// <summary>
    /// Returns the amount of damage applied to target
    /// </summary>
    public int DamagePoints => damagePoints;

    public DamageObject (Character ownerActor, Character targetActor, Attack attack)
    {
        this.ownerActor = ownerActor;
        this.targetActor = targetActor;
        this.attack = attack;
        this.isAttackCritical = this.attack.IsAttackCritical(owner.CritChance);
        this.damagePoints = DamageUtils.CalculateDamage(this.attack);

        if (ElementUtils.IsElementAdvantageAvailable(this.attack, target as EnemyCharacter))
        {
            Debug.Log("Elemental advantage applied");
            this.damagePoints += DamageUtils.CalculateElementalBonus(this.attack, target as EnemyCharacter);
        }
        if (isAttackCritical)
        {
            Debug.Log("Critical hit!");
            this.damagePoints += DamageUtils.CalculateCriticalBonus(attack);
        }
    }
}
