﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Global MonoBehavior that handles the rounds in the combat instance
/// </summary>
public class RoundManager : MonoBehaviour
{
    /// <summary>
    /// The characters spawned in combat in queue. Ordered by spawn order
    /// </summary>
    /// <typeparam name="Character">Referene to characters in combat</typeparam>
    public static List<Character> turnQueue = new List<Character>();
    /// <summary>
    /// Stack of all rounds played in combat instance
    /// </summary>
    /// <typeparam name="Round">Reference to that combat round</typeparam>
    private static Stack<Round> instanceRounds = new Stack<Round>();
    /// <summary>
    /// The current running round instance
    /// </summary>
    private Coroutine roundInstance;
    
    /// <summary>
    /// The current round
    /// </summary>
    private static Round currentRound;
    /// <summary>
    /// Returns the current round
    /// </summary>
    /// <value></value>
    public static Round CurrentRound
    {
        get => currentRound;
        set
        {
            if (currentRound != null)
                Destroy(currentRound);
            currentRound = value;
            instanceRounds.Push(CurrentRound);
        }
    }

    public delegate void RoundEvent ();
    
    /// <summary>
    /// Event handler for the start of the round
    /// </summary>
    public event RoundEvent OnRoundStart;
    /// <summary>
    /// Event handler for the end of the round
    /// </summary>
    public event RoundEvent OnRoundEnd;
    
    private void OnEnable() {
        CombatManager.OnEnemyDeath += RemoveCharacter;
    }
    private void OnDisable() {
        CombatManager.OnEnemyDeath -= RemoveCharacter;

        if (OnRoundStart != null)
            OnRoundStart = null;
        if (OnRoundEnd != null)
            OnRoundEnd = null;      
    }
    
    /// <summary>
    /// Removes the dead character from future round queues
    /// </summary>
    /// <param name="death"></param>
    public void RemoveCharacter (DeathObject death)
    {
        if (turnQueue.Contains(death.target))
            turnQueue.Remove(death.target);
    }
    /// <summary>
    /// Adds character to future round queues
    /// </summary>
    /// <param name="character"></param>
    public void AddCharacterToQueue (Character character)
    {
        turnQueue.Add(character);
    }
    /// <summary>
    /// Creates a new round then starts the round instance
    /// </summary>
    public void NextRound ()
    {
        CurrentRound = gameObject.AddComponent<Round>();        
        StartRound();
    }
    /// <summary>
    /// Starts the round instance and triggers OnRoundStart
    /// </summary>
    private void StartRound ()
    {
        roundInstance = StartCoroutine(RoundInstance());
        //  OnRoundStart();
    }
    /// <summary>
    /// Triggers OnRoundEnd and starts the next round
    /// </summary>
    private void EndRound ()
    {
        //  OnRoundEnd();
        NextRound();
    }

    /// <summary>
    /// Waits until the round is complete before moving on to the next round
    /// </summary>
    public IEnumerator RoundInstance ()
    {
        yield return new WaitUntil (
            () => CurrentRound.IsRoundComplete
        );
        EndRound();
    }
}
