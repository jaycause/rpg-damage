/// <summary>
/// Information about the death of a character
/// </summary>
public class DeathObject
{
    /// <summary>
    /// Character that dealt the final blow
    /// </summary>
    private Character ownerActor;
    /// <summary>
    /// Character that has just died
    /// </summary>
    private Character targetActor;

    /// <summary>
    /// Returns the character that dealt the final blow
    /// </summary>
    public Character owner => ownerActor;
    /// <summary>
    /// Returns the character that has just died
    /// </summary>
    public Character target => targetActor;

    public DeathObject (Character ownerActor, Character targetActor)
    {
        this.ownerActor = ownerActor;
        this.targetActor = targetActor;
    }
}