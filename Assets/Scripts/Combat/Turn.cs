﻿using System.Collections;
using UnityEngine;

/// <summary>
/// MonoBehavior that handles the character's turn behaviors
/// </summary>
public class Turn : MonoBehaviour
{
    /// <summary>
    /// The character in turn
    /// </summary>
    private Character character;
    /// <summary>
    /// Is this turn complete?
    /// </summary>
    private bool isTurnComplete = false;

    /// <summary>
    /// Returns the character in turn
    /// </summary>
    public Character CurrentCharacter => character;
    /// <summary>
    /// Returns if this character's turn is complete
    /// </summary>
    public bool IsTurnComplete => isTurnComplete;

    public delegate void TurnEvent ();
    /// <summary>
    /// Event handler for this character's start of the turn
    /// </summary>
    public event TurnEvent OnTurnStart;
    /// <summary>
    /// Event handler for this character's end of the turn
    /// </summary>
    public event TurnEvent OnTurnEnd;

    private void OnEnable() {
        this.character = GetComponent<Character>();
        Debug.Log("Current turn: <b>" + this.character.name + "</b>");
        OnTurnStart += character.TurnStart;
        OnTurnEnd += character.TurnEnd;
        CombatManager.OnCombatEnd += CancelTurn;
    }
    private void OnDisable() {
        OnTurnStart -= character.TurnStart;
        OnTurnEnd -= character.TurnEnd;
        CombatManager.OnCombatEnd -= CancelTurn;
    }

    /// <summary>
    /// Triggers OnTurnStart
    /// </summary>
    public void StartTurn ()
    {
        OnTurnStart();
    }
    /// <summary>
    /// Triggers OnTurnEnd, then ends this turn
    /// </summary>
    public void EndTurn ()
    {
        OnTurnEnd();
        isTurnComplete = true;
        Debug.Log("Turn complete");
        transform.parent.BroadcastMessage("NextTurn");
        this.enabled = false;
        Destroy(this);
    }
    /// <summary>
    /// Cancels this turn
    /// </summary>
    public void CancelTurn () {
        if (OnTurnStart != null)
            OnTurnStart = null;
        if (OnTurnEnd != null)
            OnTurnEnd = null;
        this.CancelInvoke();
    }

    // DEPRECATED
    public IEnumerator TurnInstance ()
    {
        StartTurn ();
        yield return new WaitUntil ( () => IsTurnComplete );
    }
}
