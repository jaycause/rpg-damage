﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//  TODO: Add HP bar toggle
/// <summary>
/// MonoBehavior that handles the damage popup view
/// </summary>
public class DamageView : MonoBehaviour
{
    /// <summary>
    /// Damage text object prefab
    /// </summary>
    [SerializeField]
    private GameObject damageText;
    /// <summary>
    /// The current active damage text
    /// </summary>
    private GameObject activeDamageText;
    /// <summary>
    /// Pooled damage text objects to reuse
    /// </summary>
    /// <typeparam name="GameObject">The damage text object</typeparam>
    private Queue<GameObject> pooledDamageText = new Queue<GameObject>();
    /// <summary>
    /// Motion tween parameters
    /// </summary>
    private Hashtable ht = new Hashtable();

    private void OnEnable() {
        GetComponent<Character>().OnReceiveDamage += ShowDamage;
    }
    private void OnDisable() {
        GetComponent<Character>().OnReceiveDamage -= ShowDamage;
    }

    /// <summary>
    /// Creates/updates a damage text object and plays its popup animation
    /// </summary>
    /// <param name="damage"></param>
    public void ShowDamage (DamageObject damage) {
        if (pooledDamageText.Count < 5)
        {
            activeDamageText = Instantiate(damageText, transform);
        } else {
            activeDamageText = pooledDamageText.Dequeue();
        }

        activeDamageText.GetComponent<DamageText>().SetDamageText(damage.DamagePoints);
        SetTweenHashtable ();
        iTween.MoveTo(activeDamageText, ht);
        ht.Clear();
        pooledDamageText.Enqueue(activeDamageText);
    }

    /// <summary>
    /// Sets the motion tween parameters
    /// </summary>
    private void SetTweenHashtable ()
    {
        ht.Add("y", 1.5f);
        ht.Add("time", 0.5f);
        ht.Add("easeType", "easeOutQuad");
        ht.Add("isLocal", true);
        ht.Add("oncomplete", "HideAndResetText");
        ht.Add("oncompletetarget", activeDamageText);
    }
}
