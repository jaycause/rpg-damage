﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// MonoBehavior that handles the damage text popup object
/// </summary>
public class DamageText : MonoBehaviour
{
    public void SetDamageText (int damage)
    {
        gameObject.GetComponent<TextMeshPro>().text = damage.ToString();
        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);
    }

    public void HideAndResetText () {
        gameObject.SetActive(false);
        transform.localPosition = Vector3.zero;
    }
}
