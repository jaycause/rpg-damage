﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StatusEffectInfo : ScriptableObject
{
    protected event StatusEffectEvent OnApplyStatusEffect;

    public void ApplyStatusEffect ()
    {
        OnApplyStatusEffect();
    }
}
