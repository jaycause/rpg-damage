using UnityEngine;

/// <summary>
/// A basic attack that applies damage and checks for crit
/// </summary>
[CreateAssetMenu(fileName="Base Attack", menuName="Attacks/Base Attack")]
public class BaseAttack : Attack
{

}