﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

/// <summary>
/// Abstract ScriptableObject detailing the attack's effects
/// </summary>
public abstract class Attack : ScriptableObject
{
    /// <summary>
    /// The range of damage points an attack will apply
    /// </summary>
    /// <returns>Vector2 containing the min and max damage points</returns>
    [MinMaxSlider(1, 100)]
    public Vector2 damageRange = new Vector2(1, 10);
    /// <summary>
    /// The The range of percentage an attack will add on successful critical hit
    /// </summary>
    /// <returns>The percent bonus of an attack</returns>
    [MinMaxSlider(0.01f, 0.99f)]
    public Vector2 critBonusModifier = new Vector2(0.1f, 0.2f);
    /// <summary>
    /// The attack's elemental type
    /// </summary>
    public Element elementType;

    /// <summary>
    /// Returns a bool if the attack is elemental or not
    /// </summary>
    public bool IsElemental => elementType != null;

    /// <summary>
    /// Returns if the attack is a critical hit or not
    /// </summary>
    /// <param name="critChance">The character's critical chance percentage</param>
    /// <returns>A bool if the attack is critical</returns>
    public bool IsAttackCritical (float critChance)
    {
        var roll = Random.Range(0f, 1f);
        return roll <= critChance;
    }
}
