﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageStatusEffect
{
    int DamageOnEffect { get; }

    void ApplyDamage (int damageOnEffect);
}
