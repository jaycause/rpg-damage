﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
public delegate void StatusEffectEvent ();
public abstract class StatusEffect : MonoBehaviour
{
    [SerializeField]
    protected StatusEffectInfo statusEffectInfo;
    public abstract StatusEffectInfo StatusEffectInfo { get; set; }

    public event StatusEffectEvent OnStatusEffectTrigger;

    private void OnDisable() {
        if (OnStatusEffectTrigger != null)
            OnStatusEffectTrigger -= StatusEffectInfo.ApplyStatusEffect;
    }

    public void TriggerStatusEffect ()
    {
        OnStatusEffectTrigger();
    }
    
}

//  TODO: TimeIntervalStatusEffect

public class TurnIntervalStatusEffect : StatusEffect
{
    [SerializeField] [MinMaxSlider(3f, 10f)]
    private Vector2 turnIntervalRange = new Vector2(3, 10);

    private int? turnsForEffect;

    private int TurnsForEffect
    {
        get {
            if (turnsForEffect == null)
                turnsForEffect = GetTurnsForEffect(turnIntervalRange);
            return turnsForEffect.GetValueOrDefault();
        }
        set {
            turnsForEffect = value;
            if (TurnsForEffect == 0)
                Destroy(this);
        }
    }

    public override StatusEffectInfo StatusEffectInfo
    {
        get => statusEffectInfo;
        set {
            statusEffectInfo = value;
            //  FIXME: handle event changes when the status effect changes
            OnStatusEffectTrigger += statusEffectInfo.ApplyStatusEffect;
            turnsForEffect = GetTurnsForEffect(turnIntervalRange);
        }
    }

    public void TriggerStatusEffectOnTurn ()
    {
        TriggerStatusEffect();
        TurnsForEffect --;
    }

    private int GetTurnsForEffect (Vector2 turnIntervalRange)
    {
        return Mathf.RoundToInt(Random.Range(turnIntervalRange.x, turnIntervalRange.y));
    }

}