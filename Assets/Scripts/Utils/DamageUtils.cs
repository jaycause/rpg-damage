﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Static class of Damage utility functions
/// </summary>
public static class DamageUtils
{
    //  TODO: modifier for target defense
    //  TODO: modifier for target evasion

    /// <summary>
    /// Calculates the damage of a landing attack
    /// </summary>
    /// <param name="attack">Attack being used</param>
    /// <returns>The generated damage amount</returns>
    public static int CalculateDamage (Attack attack)
    {
        return Mathf.RoundToInt(Random.Range(attack.damageRange.x, attack.damageRange.y));
    }
    /// <summary>
    /// Calculates the additional damage points on critical hit
    /// </summary>
    /// <param name="attack">Attack being used</param>
    /// <returns>The generated critical damage bonus</returns>
    public static int CalculateCriticalBonus (Attack attack)
    {
        int baseDamage = CalculateDamage(attack);
        float critBonus = baseDamage * Random.Range(attack.critBonusModifier.x, attack.critBonusModifier.y);
        return Mathf.RoundToInt(critBonus);
    }
    /// <summary>
    /// Calculates the elemental damage bonus if there is an advantage
    /// </summary>
    /// <param name="attack">Attack being used</param>
    /// <param name="target">Target character</param>
    /// <returns>The generated damage bonus on elemental advantage/disadvantage</returns>
    public static int CalculateElementalBonus (Attack attack, Character target)
    {
        if (target is EnemyCharacter)
        {
            var enemyTarget = target as EnemyCharacter;
            if (ElementUtils.StrongerElement(attack.elementType, enemyTarget.ElementType) == attack.elementType)
                return CalculateCriticalBonus(attack);
            else if (ElementUtils.StrongerElement(attack.elementType, enemyTarget.ElementType) == enemyTarget.ElementType)
                return -(CalculateCriticalBonus(attack));
            else
                return 0;
        }
        //  TODO: add Player condition
        return 0;
    }

    //  DEPRECATED
    public static bool IsAttackCritical (float critChance)
    {
        var roll = Random.Range(0f, 1f);
        return roll <= critChance;
    }
}
