﻿/// <summary>
/// Static class of Element utility functions
/// </summary>
public static class ElementUtils
{
    //  FIXME: theres gotta be a better way to go about that
    //  revisit when importing OdinInspector
    /// <summary>
    /// Determines which of the two elements are stronger
    /// </summary>
    /// <param name="e1">Element 1</param>
    /// <param name="e2">Element 2</param>
    /// <returns>The stronger element</returns>
    public static Element StrongerElement(Element e1, Element e2)
    {
        if (e1.HasAdvantageOver(e2))
            return e1;
        if (e2.HasAdvantageOver(e1))
            return e2;
        return null;
    }
    
    //  TODO: make sure elemental type works on player depending on their elemental equips
    /// <summary>
    /// Checks if a character or attack has an elemental bonus
    /// </summary>
    /// <param name="attack">Attack being used</param>
    /// <param name="target">The target character</param>
    /// <returns>True if both parameters are elemental and can be compared</returns>
    public static bool IsElementAdvantageAvailable (Attack attack, Character target)
    {
        if (target is EnemyCharacter)
        {
            var enemyTarget = target as EnemyCharacter;
            if (attack.IsElemental && enemyTarget.IsElemental)
                return true;
            return false;
        }
        //  TODO: add Player condition
        return false;
    }
}
