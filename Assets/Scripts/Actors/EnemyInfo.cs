using UnityEngine;
using NaughtyAttributes;

//  TODO: Create different enemy infos for different enemies
/// <summary>
/// ScriptableObject containing information about an enemy entity
/// </summary>
public class EnemyInfo : CharacterInfo
{
    /// <summary>
    /// The range of HP an enemy can start with
    /// </summary>
    /// <returns>Vector2 containing the minimum and maximum HP</returns>
    [MinMaxSlider(10, 100)]
    private Vector2 healthRange = new Vector2(30, 60);
    /// <summary>
    /// The range of crit chance percentage an enemy can start with
    /// </summary>
    /// <returns>Vector2 containing the minimum and maximum crit chance</returns>
    [MinMaxSlider(0.01f, 0.2f)]
    private Vector2 critChanceRange = new Vector2(0.05f, 0.1f);
    /// <summary>
    /// The enemy's elemental type
    /// </summary>
    [SerializeField]
    private Element elementType;
    /// <summary>
    /// Returns the enemy's elemental type
    /// </summary>
    /// <value></value>
    public Element ElementType
    {
        get => this.elementType;
        set => this.elementType = value;
    }

    private void Awake() {
        health = GenerateHealth();
        criticalChance = GenerateCritChance();
    }

    /// <summary>
    /// Generates the enemy's starting HP using their set HP range
    /// </summary>
    /// <returns>The finalized starting HP of the enemy when it spawns in combat</returns>
    private int GenerateHealth ()
    {
        return Mathf.RoundToInt(Random.Range(healthRange.x, healthRange.y));
    }
    /// <summary>
    /// Generates the enemy's starting crit chance using their set crit chance range
    /// </summary>
    /// <returns>The finalized starting crit chance of the enemy when it spawns in combat</returns>
    private float GenerateCritChance ()
    {
        return Random.Range(critChanceRange.x, critChanceRange.y);
    }
}