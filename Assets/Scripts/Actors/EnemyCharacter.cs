﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Entity MonoBehavior for combat enemy NPCs
/// </summary>
public class EnemyCharacter : Character
{
#region Attributes
    /// <summary>
    /// The enemy's stats information
    /// </summary>
    [SerializeField]
    private EnemyInfo enemyInfo;
    /// <summary>
    /// The enemy's elemental type
    /// </summary>
    [SerializeField] [Tooltip("Set to give enemy an elemental type. Keep empty if not elemental.")]
    private Element elementType;
#endregion

#region Properties
    /// <summary>
    /// Returns the enemy's current health
    /// </summary>
    public override int Health => enemyInfo.health;
    /// <summary>
    /// Returns the enemy's maximum health
    /// </summary>
    public override int MaxHealth => enemyInfo.maxHealth;
    /// <summary>
    /// Returns the enemy's percent chance of landing a critical hit
    /// </summary>
    public override float CritChance => enemyInfo.criticalChance;
    /// <summary>
    /// Returns the enemy's elemental type
    /// </summary>
    public Element ElementType => enemyInfo.ElementType;
    /// <summary>
    /// Returns if the enemy is elemental or not
    /// </summary>
    public bool IsElemental => this.ElementType != null;
#endregion

#region MonoBehaviors
    private void Start() {
        enemyInfo = this.CreateEnemyInfo();
        if (this.elementType != null)
            enemyInfo.ElementType = this.elementType;
        CombatManager.HandleSpawn(this);
    }
#endregion

#region Functions
    /// <summary>
    /// Calls the enemy's OnTurnStart
    /// </summary>
    public override void TurnStart ()
    {
        //  FIXME: implement enemy AI
        //  FIXME: might be more effective to call an event to ensure turn functions are only called when enabled
        Debug.Log(gameObject.name + " received their turn");
        gameObject.GetComponent<Turn>().EndTurn();
    }
    /// <summary>
    /// Calls the enemy's OnTurnEnd
    /// </summary>
    public override void TurnEnd ()
    {
         //  TODO: implement enemy AI
    }

    /// <summary>
    /// Generates the enemy's information on combat start
    /// </summary>
    /// <returns>A new EnemyInfo instance for that enemy</returns>
    private EnemyInfo CreateEnemyInfo ()
    {
        return ScriptableObject.CreateInstance<EnemyInfo>();
    }
    /// <summary>
    /// Calls enemyInfo.ReceiveDamagePoints(damage)
    /// </summary>
    /// <param name="damage">The DamageObject created on successful hit</param>
    protected override void ReceiveDamagePoints(DamageObject damage)
    {
        enemyInfo.ReceiveDamagePoints(damage);
    }
#endregion
}
