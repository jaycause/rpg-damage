using UnityEngine;

//  TODO: add slots for Attacks, Items, Weapons, and Armor
/// <summary>
/// Entity MonoBehavior for player(s) in combat
/// </summary>
public class PlayerCharacter : Character
{
#region Attributes
    /// <summary>
    /// The character's stats information
    /// </summary>
    [SerializeField]
    private CharacterInfo characterInfo;
#endregion

#region Properties
    /// <summary>
    /// Returns the player's current health
    /// </summary>
    public override int Health => characterInfo.health;
    /// <summary>
    /// Returns the player's maximum health
    /// </summary>
    public override int MaxHealth => characterInfo.maxHealth;
    /// <summary>
    /// Returns the player's percent chance of landing a critical hit
    /// </summary>
    public override float CritChance => characterInfo.criticalChance;
#endregion

#region MonoBehaviors
    private void Start() {
        CombatManager.HandleSpawn(this);
    }
#endregion

#region Functions
    /// <summary>
    /// Calls the player's OnTurnStart
    /// </summary>
    public override void TurnStart ()
    {
        // FIXME: change this into event
        Debug.Log("Player's turn");
    }
    /// <summary>
    /// Calls the player's OnTurnEnd
    /// </summary>
    public override void TurnEnd ()
    {
        // FIXME: change this into event
        
    }

    /// <summary>
    /// Calls characterInfo.ReceiveDamagePoints(damage)
    /// </summary>
    /// <param name="damage">The DamageObject created on successful hit</param>
    protected override void ReceiveDamagePoints (DamageObject damage)
    {
        characterInfo.ReceiveDamagePoints(damage);
    }
#endregion
}