﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Entity MonoBehavior handling combat events and character info functions
/// </summary>
public abstract class Character : MonoBehaviour
{
#region Properties
    /// <summary>
    /// The character's current health
    /// </summary>
    /// <value>The character's current health</value>
    public abstract int Health { get; }
    /// <summary>
    /// The character's maximum health
    /// </summary>
    /// <value>The character's maximum health</value>
    public abstract int MaxHealth { get; }
    /// <summary>
    /// The character's percent chance of landing a critical hit
    /// </summary>
    /// <value>The character's percent chance of landing a critical hit</value>
    public abstract float CritChance { get; }
#endregion

#region Events
    public delegate void DamageEvent(DamageObject damage);
    public delegate void DeathEvent(DeathObject death);
    public delegate void TurnEvent ();
    /// <summary>
    /// Event handler for the character receiving damage
    /// </summary>
    public event DamageEvent OnReceiveDamage;
    /// <summary>
    /// Event handler for the character that just died
    /// </summary>
    public event DeathEvent OnDeath;
    /// <summary>
    /// Event handler for the start of the character's turn
    /// </summary>
    public event TurnEvent OnTurnStart;
    /// <summary>
    /// Event handler for the end of the character's turn
    /// </summary>
    public event TurnEvent OnTurnEnd;
    /// <summary>
    /// Event handler for when the character's turn is paused
    /// </summary>
    public event TurnEvent OnTurnPaused;
    /// <summary>
    /// Event handler for when the character's turn is cancelled
    /// </summary>
    public event TurnEvent OnTurnCancelled;
#endregion

#region MonoBehaviors
    protected void OnEnable() {
        OnReceiveDamage += ReceiveDamagePoints;
        OnDeath += CombatManager.HandleDeath;
    }
    protected void OnDisable() {
        OnReceiveDamage -= ReceiveDamagePoints;
        OnDeath -= CombatManager.HandleDeath;
    }
#endregion

#region Functions
    /// <summary>
    /// Triggers the OnReceiveDamage event and CheckForDeath() function
    /// </summary>
    /// <param name="damage">The DamageObject created on successful hit</param>
    public void ReceiveDamage (DamageObject damage)
    {
        OnReceiveDamage(damage);
        CheckForDeath(damage);
    }
    /// <summary>
    /// Applies damage points to the character
    /// </summary>
    /// <param name="damage">The DamageObject created on successful hit</param>
    protected abstract void ReceiveDamagePoints (DamageObject damage);

    /// <summary>
    /// Triggers the OnTurnStart event when the character's turn starts
    /// </summary>
    public abstract void TurnStart ();
    /// <summary>
    /// Triggers the OnTurnEnd event when the character's turn ends
    /// </summary>
    public abstract void TurnEnd ();

    /// <summary>
    /// Set's the character's Active to false
    /// </summary>
    public void DeactivateCharacter ()
    {
        gameObject.SetActive(false);
    }
    /// <summary>
    /// Checks if this character is dead
    /// </summary>
    /// <param name="damage">The DamageObject created on successful hit</param>
    private void CheckForDeath (DamageObject damage)
    {
        if (Health <= 0)
        {
            OnDeath(new DeathObject(damage.owner, this));
        }
    }
#endregion
}
