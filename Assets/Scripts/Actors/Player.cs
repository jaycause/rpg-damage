﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//  TODO: determine character's representing element type by their majority equipment element type
public class Player : MonoBehaviour
{
    public Attack attack;

    private void Update() {
        if (Input.GetMouseButtonDown(0))
            Attack();
    }

    private void Attack () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (gameObject.GetComponent<Turn>() != null)
        {
            if (Physics.Raycast(ray, out hit))
            {
                DamageObject damage = new DamageObject(GetComponent<Character>(), hit.transform.GetComponent<Character>(), attack);
                hit.transform.BroadcastMessage("ReceiveDamage", damage);
            } else {
                Debug.Log("Missed");
            }

            gameObject.GetComponent<Turn>().EndTurn();
        }
    }


}
