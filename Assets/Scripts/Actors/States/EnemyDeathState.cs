﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The behavior used upon playing the enemy's Death animation state
/// </summary>
public class EnemyDeathState : StateMachineBehaviour
{
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var enemyChar = animator.gameObject.GetComponent<EnemyCharacter>();
        enemyChar.DeactivateCharacter();
    }

}
