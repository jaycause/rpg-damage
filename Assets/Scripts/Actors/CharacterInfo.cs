﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

/// <summary>
/// ScriptableObject containing information about a character entity
/// </summary>
[CreateAssetMenu(fileName="Character Info", menuName="Character/Character Info")]
public class CharacterInfo : ScriptableObject
{
    /// <summary>
    /// The maximum health a character starts with
    /// </summary>
    [Range(0, 100)]
    public int maxHealth = 50;
    /// <summary>
    /// The current health a character has
    /// </summary>
    public int health;
    /// <summary>
    /// Percent chance that the character will land a critical hit
    /// </summary>
    [Range(0.01f, 0.99f)]
    public float criticalChance = 0.2f;

    private void Awake() {
        health = maxHealth;
    }

    /// <summary>
    /// Applies damage points to the character
    /// </summary>
    /// <param name="damage">The DamageObject created on successful hit</param>
    public void ReceiveDamagePoints(DamageObject damage)
    {
        health -= damage.DamagePoints;
    }
}
