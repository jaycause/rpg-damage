﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// MonoBehavior that controls the enemy's animation controller
/// </summary>
public class EnemyAnimationController : MonoBehaviour
{
    /// <summary>
    /// The enemy's animation controller
    /// </summary>
    private Animator animator;
    /// <summary>
    /// The enemy character
    /// </summary>
    private EnemyCharacter enemyChar;

    private void Awake() {
        animator = GetComponent<Animator>();
        enemyChar = GetComponent<EnemyCharacter>();
    }
    private void OnEnable() {
        enemyChar.OnReceiveDamage += PlayHit;
        enemyChar.OnDeath += PlayDeath;
    }
    private void OnDisable() {
        enemyChar.OnReceiveDamage -= PlayHit;
        enemyChar.OnDeath -= PlayDeath;
    }

    /// <summary>
    /// Sets the IsHit trigger
    /// </summary>
    /// <param name="damage"></param>
    private void PlayHit (DamageObject damage)
    {
        animator.SetTrigger("IsHit");
    }
    /// <summary>
    /// Sets the IsDead trigger
    /// </summary>
    /// <param name="death"></param>
    private void PlayDeath (DeathObject death)
    {
        animator.ResetTrigger("IsHit");
        animator.SetTrigger("IsDead");
    }
    
}
