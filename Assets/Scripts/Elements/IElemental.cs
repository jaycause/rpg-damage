﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//  TODO: probably use this for equipment element modifiers
public interface IElementalItem
{
    Element ElementType { get; }
}


public interface IElementalEffect
{
    Element ElementType { get; }
    
    void ApplyElement (Element element);
}
