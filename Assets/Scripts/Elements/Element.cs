﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ScriptableObject that details the element type
/// </summary>
[CreateAssetMenu(fileName="Element")] [ExecuteInEditMode]
public class Element : ScriptableObject
{
    public List<Element> hasAdvantageOver = new List<Element>();

    public bool HasAdvantageOver (Element e)
    {
        return hasAdvantageOver.Contains(e);
    }

}
